# https://www.hackerrank.com/challenges/ctci-ransom-note/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=dictionaries-hashmaps
import math
import os
import random
import re
import sys

# Complete the checkMagazine function below.
def checkMagazine(magazine, note):
    mag_dict = {}
    note_dict = {}
    # Read magzine once
    for x in magazine:
        if x in mag_dict:
            mag_dict[x]+=1
        else:
            mag_dict.update({x:1})
    # Read note once
    for y in note:
        if y in note_dict:
            note_dict[y]+=1
        else:
            note_dict.update({y:1})
    result=True
    for key,val in note_dict.items():
        if key not in mag_dict:
            return False
        elif note_dict[key]>mag_dict[key]:
            return False
    return result

if __name__ == '__main__':
    mn = input().split()

    m = int(mn[0])

    n = int(mn[1])

    magazine = input().rstrip().split()

    note = input().rstrip().split()

    if checkMagazine(magazine, note):
        print("Yes")
    else:
        print("No")
