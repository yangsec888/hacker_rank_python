import os
import sys

#
# Complete the timeConversion function below.
#
def timeConversion(s):
    #
    time=s.split(":")
    if "PM" in s:
        hr=int(time[0])+12
        if hr==24:
            time[0]="12"
        else:
            time[0]=str(hr)
    elif time[0]=="12":
        time[0]="00"
    return ":".join(time)[:-2]


if __name__ == '__main__':
    f = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    result = timeConversion(s)

    f.write(result + '\n')

    f.close()
