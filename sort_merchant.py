# https://www.hackerrank.com/challenges/sock-merchant/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=warmup
import math
import os
import sys

# Complete the sort merchant function below.
def sort_merchant(ar):
    pairs=0
    merchant={}
    for a in ar:
        if a in merchant.keys():
            merchant[a]+=1
        else:
            merchant[a]=1
    for b in merchant.values():
        pairs+= b//2
    return pairs

if __name__ == '__main__':

    ar_count = int(input())
    ar = list(map(int, input().rstrip().split()))
    result = sort_merchant(ar)
    print(result)
