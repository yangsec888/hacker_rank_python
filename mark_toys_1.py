#!/bin/python3
# https://www.hackerrank.com/challenges/mark-and-toys/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=sorting&h_r=next-challenge&h_v=zen

import math
import os
import random
import re
import sys

if __name__ == '__main__':
    [n,budget] = list(map(int, input().rstrip().split()))
    a = list(map(int, input().rstrip().split()))
    a.sort()
    cnt=0
    for x in a:
        budget-=x
        if budget>0:
            cnt+=1
        else:
            break
    print(cnt)
