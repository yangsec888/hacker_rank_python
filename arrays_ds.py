import math
import os
import random
import re
import sys

# Complete the reverseArray function below.
def reverseArray(a):
    b=a[::-1]
    return b

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    arr_count = int(input())

    arr = list(map(int, input().rstrip().split()))
    print("arr:", arr)
    res = reverseArray(arr)
    print("res:", res)
    fptr.write(' '.join(map(str, res)))
    fptr.write('\n')

    fptr.close()
