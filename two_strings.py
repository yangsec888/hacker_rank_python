# https://www.hackerrank.com/challenges/two-strings/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=dictionaries-hashmapsimport math
# Given two strings, determine if they share a common substring. A substring may be as small as one character.
import os
import sys

# Complete the string comparison function below.
def comp_str(s1,s2):
    return True if set(s1)&set(s2) else False

if __name__ == '__main__':
    pairs = int(input())
    inputs = []
    for x in range(pairs):
      s1 = input().strip()
      s2 = input().strip()
      inputs.append([s1,s2])
    for x,y in inputs:
        if (comp_str(x,y)):
          print("YES")
        else:
          print("NO")
