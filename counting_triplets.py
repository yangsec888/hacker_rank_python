import os
import sys

# Complete the countTriplets function below.
def countTriplets(arr, r):
    global n
    # hashmap holding the potential triplets: 1 2 2 4 => {1: [1, 2, 1], 2: [2, 1, 0], 4: [1, 0, 0]}
    triplets={}
    arr.sort()
    for x in range(n):             # parse the array one time and build the hashmap O(N)
        if arr[x] % pow(r,2) == 0:
            bs2 = arr[x]//pow(r,2) # case for second order
            triplets[bs2]=triplets.get(bs2,[0,0,0])
            triplets[bs2][2]+=1
            bs1 = arr[x]//pow(r,1) # case for first order
            triplets[bs1]=triplets.get(bs1,[0,0,0])
            triplets[bs1][1]+=1
            bs0 = arr[x]//pow(r,0) # case for zero order
            triplets[bs0]=triplets.get(bs0,[0,0,0])
            triplets[bs0][0]+=1
        elif arr[x] % pow(r,1) == 0:
            bs1 = arr[x]//pow(r,1) # case for first order
            triplets[bs1]=triplets.get(bs1,[0,0,0])
            triplets[bs1][1]+= 1
            bs0 = arr[x]//pow(r,0) # case for Zero order
            triplets[bs0]=triplets.get(bs0,[0,0,0])
            triplets[bs0][0]+= 1
        else:
            bs0 = arr[x]           # case for zero order
            if bs0 in triplets:
                triplets[bs0][0]+=1
            else:
                triplets[bs0]=[1,0,0]
    cnt=0 # counting on the hashmap one time
    for entry in triplets.values():
        if entry[0]>0 and entry[1]>0 and entry[2]>0:
            cnt+=sum(entry)-2
    return cnt

if __name__ == '__main__':
    inputs = input().strip().split()
    n = int(inputs[0])
    r = int(inputs[1])
    arr = list(map(int, input().rstrip().split()))
    print(countTriplets(arr,r))
