#!/usr/bin/env python3
# https://www.hackerrank.com/challenges/new-year-chaos/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=arrays

import math
import os
import random
import re
import sys
sys.setrecursionlimit(10000)

# Complete the rotRight function below.
def chaos_sort(stage,base):
    global bribe_cnt
    global chaotic
    for y in range(len(stage)):
        if (stage[y] - base[y]) > 2:
            chaotic=True
            return
        elif stage[y] > stage[y+1]:
            pos=y
            break
        else:
            pos=y
    if pos < len(stage)-1:
        (stage[pos],stage[pos+1])=(stage[pos+1],stage[pos]) # rotate right
        bribe_cnt+=1
    #print("One move:",stage)
    #print("bribe_cnt:",bribe_cnt)
    if stage == base:
        return
    else:
        chaos_sort(stage,base)

def file_2_list(file):
    my_list=[]
    with open(file,'r') as f:
        line=f.readline()
        while line:
            my_list.append(line.rstrip())
            line=f.readline()
    f.close()
    return my_list

if __name__ == '__main__':
    file=str(input())
    input_list= file_2_list(file)
    #print("Input_list",input_list)
    test_cases= int(input_list[0])
    stages=[]
    # Data input handling
    for x in range(test_cases):
        size_queue = int(input_list[1+x*2])
        my_stage=list(input_list[2+x*2].split(" "))[:size_queue]
        my_stage= list(map(int, my_stage))
        stages.append(my_stage)
        #print("size: ",size_queue, "stage:", my_stage)
    # Data processing
    for stage in stages:
        bribe_cnt=0
        chaotic=False
        base=[x+1 for x in range(len(stage))]
        chaos_sort(stage, base)
        if chaotic:
            print("Too chaotic")
        else:
            print(bribe_cnt)
