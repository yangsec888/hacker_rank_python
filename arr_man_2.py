#!/usr/bin/env python3
# https://www.hackerrank.com/challenges/crush/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=arrays

import math
import os
import random
import re
import sys

# Complete the arrayManipulation function below.
def arrayManipulation(n, queries):
    base=[0 for x in range(n+1)]
    for query in queries:
        base[int(query[0])-1]+=int(query[2])
        base[int(query[1])]-=int(query[2])
    idx=0
    man=[]
    for y in base:
        if idx==0:
            man.append(y)
        else:
            man.append(man[-1]+y)
        idx+=1
    return max(man)

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    nm = input().split()

    n = int(nm[0])

    m = int(nm[1])

    queries = []

    for _ in range(m):
        queries.append(list(map(int, input().rstrip().split())))

    result = arrayManipulation(n, queries)

    fptr.write(str(result) + '\n')
