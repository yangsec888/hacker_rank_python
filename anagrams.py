# https://www.hackerrank.com/challenges/sherlock-and-anagrams/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=dictionaries-hashmaps
import os
import sys

# string to dictionary converstion: 'abba' => {'a':2, 'b':2}
def str_to_dic(s1):
    dict={}
    for x in list(s1):
        if x in dict:
            dict[x]+=1
        else:
            dict[x]=1
    return dict

# anagrams evaluation function
def are_anagrams(s1,s2):
    return True if str_to_dic(s1) == str_to_dic(s2) else False

if __name__ == '__main__':
    entries = int(input())
    inputs = []
    for _ in range(entries):
        inputs.append(input().strip())
    for my_string in inputs:
        sub_strs={}
        i=0
        #build full substring in hash map: 'abba' => {1: 'a', 2: 'ab', 3: 'abb', 4: 'abba', 5: 'b', 6: 'bb', 7: 'bba', 8: 'b', 9: 'ba'}
        for x in range(len(my_string)):
            for y in range(x,len(my_string)):
                sub_strs.update({i:my_string[x:y+1]})
                i+=1
        cnt=0
        #print(sub_strs)
        #iterate through the hash map and count the anagrams once found
        for key1,val1 in sub_strs.items():
            for key2 in range(key1+1,len(sub_strs)):
                if len(val1)==len(sub_strs[key2]):
                    if are_anagrams(val1,sub_strs[key2]):
                        cnt+=1
        print(cnt)
