#https://www.hackerrank.com/challenges/repeated-string/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=warmup&h_r=next-challenge&h_v=zen
def count(s,n):
    mul = n//len(s)
    mo = n % len(s)
    cnt1=cnt2=0
    for x in list(s):
        if x== "a":
            cnt1+=1
    for y in list(s[:mo]):
        if y=="a":
            cnt2+=1
    return cnt1*(n//len(s))+cnt2

if __name__ == "__main__":
    s=str(input())
    n=int(input())
    print(count(s,n))
