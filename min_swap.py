#!/usr/bin/env python3
# https://www.hackerrank.com/challenges/minimum-swaps-2/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=arrays

import math
import os
import random
import re


# Complete the min swap function below.
def min_swap(arr,base):
    global cnt
    #print("start again")
    if arr==base:
        return
    # min swap algo step 1: perfect swap move first
    for idx,x in enumerate(arr):
        #print("Element idx:",idx, "Element:",x)
        if (x - 1 - idx) != 0 and idx<(len(arr)-1):
            if arr[x-1] == arr[idx]: #looking for perfect swap move first
                arr[x-1],arr[idx]=arr[idx],arr[x-1]
                cnt+=1
                #min_swap(arr,base)
    # min swap algo step 2: perfect swap move first
    for idx,x in enumerate(arr):
        if (x - 1 - idx) != 0:
            arr[idx],arr[x-1]=arr[x-1],arr[idx]
            cnt+=1
    min_swap(arr,base)

if __name__ == '__main__':
    cnt=0
    size = int(input())
    arr = list(map(int, input().rstrip().split()))[:size]
    base=[x+1 for x in range(len(arr))]
    min_swap(arr,base)
    print(cnt)
