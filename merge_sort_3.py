#!/usr/bin/env python3
# https://www.hackerrank.com/challenges/ctci-merge-sort/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=sorting
import math
import os
import random
import re
import sys

def file_2_list(file):
    my_list=[]
    with open(file,'r') as f:
        line=f.readline()
        while line:
            my_list.append(line.rstrip())
            line=f.readline()
    f.close()
    return my_list


def merge(a1, a2):
    swaps, i, j, result, m, n = 0, 0, 0, [], len(a1), len(a2)
    ra = result.append
    while i < m and j < n:
        if a1[i] <= a2[j]:
            ra(a1[i])
            i += 1
        else:
            ra(a2[j])
            j += 1
            swaps += m - i
    result += a1[i:]
    result += a2[j:]
    return swaps, result

# Complete the countInversions function below.
def countInversions(arr):
    n = len(arr)
    mid = n // 2
    if n > 1:
        left_swaps, left_result = countInversions(arr[:mid])
        right_swaps, right_result = countInversions(arr[mid:])
        m_swaps, result = merge(left_result, right_result)
        return m_swaps + left_swaps + right_swaps, result
    return 0, arr


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')
    input_arr = file_2_list(input())
    t = int(input_arr[0])

    i = 0
    for t_itr in range(t):
        n = int(input_arr[i+1])

        arr = list(map(int, input_arr[i+2].rstrip().split()))

        result, ar = countInversions(arr)

        fptr.write(str(result) + '\n')
        i+=2

    fptr.close()
