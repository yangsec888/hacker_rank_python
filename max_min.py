#!/bin/python3
# https://www.hackerrank.com/challenges/angry-children/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=greedy-algorithms

import math
import os
import random
import re
import sys

# Complete the maxMin function below.
def maxMin(k, arr):
    delta=list()
    arr.sort()
    for i in range(1,len(arr)):
        delta.append(arr[i]-arr[i-1])
    min = sum(delta[:k-1])
    for i in range(1,len(delta)-k):
        cur = sum(delta[i:i+k-1])
        if cur < min:
            min = cur
    return min

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    k = int(input())

    arr = []

    for _ in range(n):
        arr_item = int(input())
        arr.append(arr_item)

    result = maxMin(k, arr)
    print(result)
    fptr.write(str(result) + '\n')
