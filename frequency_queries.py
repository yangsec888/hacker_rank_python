#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the freqQuery function below.
def freqQuery(queries):
    result=[]
    stack={}
    for query in queries:
        if query[0]==1:
            stack[query[1]]=stack.get(query[1],0)+1
        elif query[0]==2:
            if query[1] in stack and stack[query[1]]>0:
                stack[query[1]]-=1
        elif query[0]==3:
            if query[1] in stack.values():
                result.append(1)
            else:
                result.append(0)
        #print(stack)
    return result


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    q = int(input().strip())

    queries = []

    for _ in range(q):
        queries.append(list(map(int, input().rstrip().split())))

    ans = freqQuery(queries)

    fptr.write('\n'.join(map(str, ans)))
    fptr.write('\n')

    fptr.close()
