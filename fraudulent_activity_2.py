#!/bin/python3
# https://www.hackerrank.com/interview/interview-preparation-kit/sorting/challenges

import math
import os
import random
import re
import sys

# conveert file to a list
def file_2_list(file):
    my_list=[]
    with open(file,'r') as f:
        line=f.readline()
        while line:
            my_list.append(line.rstrip())
            line=f.readline()
    f.close()
    return my_list

# custom insert sort algo
def insert_sort(a,itm):
    if itm<=a[0]:
        a.insert(0,itm)
        return a
    elif itm >= a[-1]:
        a.insert(len(a),itm)
        return a
    else:
        for idx in range(1,len(a)):
            if itm>= a[idx] and itm <=a[idx+1]:
                a.insert(idx,itm)
                return a

# Complete the fraud_notice function below.
def fraud_notice(a):
    global n,d
    cnt=0
    for idx in range(d,n):
        exp = a[idx]
        if idx==d:
            exps = a[idx-d:idx]
            exps.sort()
        else:
            exp0 = a[idx-d-1]
            exp1 = a[idx-1]
            exps.remove(exp0)
            exps = insert_sort(exps,exp1)
            exps.sort()
        if d%2 == 1:
            med=exps[d//2]
        else:
            med=(exps[d//2-1]+exps[d//2])/2
        if exp >= 2*med:
            cnt+=1
    return cnt


if __name__ == '__main__':
    file=str(input())
    input_list= file_2_list(file)
    #print("Input_list",input_list)
    [n,d] = list(map(int, input_list[0].split(' ')))
    a = list(map(int, input_list[1].rstrip().split(' ')))
    print(fraud_notice(a))
