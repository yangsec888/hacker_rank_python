#!/usr/bin/env python3
# https://www.hackerrank.com/challenges/new-year-chaos/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=arrays

import math
import os
import random
import re
import sys
sys.setrecursionlimit(100000)

# Complete the rotRight function below.
def chaos_sort(stage,base):
    global bribe_cnt
    global chaotic
    for y in range(len(stage)):
        if (stage[y] - base[y]) > 2:
            chaotic=True
            return
        elif stage[y] > stage[y+1]:
            pos=y
            break
        else:
            pos=y
    if pos < len(stage)-1:
        (stage[pos],stage[pos+1])=(stage[pos+1],stage[pos]) # rotate right
        bribe_cnt+=1
    #print("One move:",stage)
    #print("bribe_cnt:",bribe_cnt)
    if stage == base:
        return
    else:
        chaos_sort(stage,base)

if __name__ == '__main__':
    test_cases=int(input())
    stages=[]
    for _ in range(test_cases):
        size_queue = int(input())
        stages.append(list(map(int, input().rstrip().split()))[:size_queue])
    for stage in stages:
        bribe_cnt=0
        chaotic=False
        base=[x+1 for x in range(len(stage))]
        chaos_sort(stage, base)
        if chaotic:
            print("Too chaotic")
        else:
            print(bribe_cnt)
