#!/bin/python3
# https://www.hackerrank.com/challenges/luck-balance/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=greedy-algorithms

import math
import os
import random
import re
import sys

# Complete the luckBalance function below.
def luckBalance(k, contests):
    imp_cnt=list()
    n_cnt=list()
    for contest in contests:
        if contest[1]==1:
            imp_cnt.append(contest[0])
        else:
            n_cnt.append(contest[0])
    imp_cnt.sort()
    total = sum(n_cnt)
    if len(imp_cnt)-k>0:
        for i in range(len(imp_cnt)-k):
            total-=imp_cnt[i]
        for j in range(len(imp_cnt)-k,len(imp_cnt)):
            total+=imp_cnt[j]
    else:
        total += sum(imp_cnt)
    return total

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    nk = input().split()

    n = int(nk[0])

    k = int(nk[1])

    contests = []

    for _ in range(n):
        contests.append(list(map(int, input().rstrip().split())))

    result = luckBalance(k, contests)
    print(result)
    fptr.write(str(result) + '\n')

    fptr.close()
