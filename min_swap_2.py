#!/usr/bin/env python3
# https://www.hackerrank.com/challenges/minimum-swaps-2/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=arrays

import math
import os
import random
import re
import sys
sys.setrecursionlimit(50000)

def file_2_list(file):
    my_list=[]
    with open(file,'r') as f:
        line=f.readline()
        while line:
            my_list.append(line.rstrip())
            line=f.readline()
    f.close()
    return my_list

# Complete the min swap function below.
def min_swap(arr,base):
    global cnt
    #print("start again")
    if arr==base:
        return
    # min swap algo step 1: perfect swap move first
    for idx,x in enumerate(arr):
        #print("Element idx:",idx, "Element:",x)
        if (x - 1 - idx) != 0 and idx<(len(arr)-1):
            if arr[x-1] == arr[idx]: #looking for perfect swap move first
                arr[x-1],arr[idx]=arr[idx],arr[x-1]
                cnt+=1
                #min_swap(arr,base)
    # min swap algo step 2: perfect swap move first
    for idx,x in enumerate(arr):
        if (x - 1 - idx) != 0:
            arr[idx],arr[x-1]=arr[x-1],arr[idx]
            cnt+=1
            #min_swap(arr,base)
    min_swap(arr,base)

if __name__ == '__main__':
    cnt=0
    file=str(input())
    input_list= file_2_list(file)
    #print("Input_list",input_list)
    size = int(input_list[0])
    arr = list(map(int, input_list[1].rstrip().split()))[:size]
    base=[x+1 for x in range(len(arr))]
    min_swap(arr,base)
    print(cnt)
