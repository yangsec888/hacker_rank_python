#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the hourglassSum function below.
def hourglassSum(arr):
    print("arr:",arr)
    res_arr=[["" for _ in range(len(arr)-2)] for _ in range(len(arr)-2)]
    print("res_arr:",res_arr)
    for x in range(len(arr)-2):
        for y in range(len(arr)-2):
            res_arr[x][y] = arr[x][y]+arr[x][y+1]+arr[x][y+2]+arr[x+1][y+1]+\
                            arr[x+2][y]+arr[x+2][y+1]+arr[x+2][y+2]
    print("res_arr:",res_arr)
    max=res_arr[0][0]
    for a in range(len(res_arr)):
        for b in range(len(res_arr)):
            if res_arr[a][b]>max:
                max=res_arr[a][b]
    return max

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')
    arr = []
    for _ in range(6):
        arr.append(list(map(int, input().rstrip().split())))
    result = hourglassSum(arr)
    fptr.write(str(result) + '\n')
    fptr.close()
