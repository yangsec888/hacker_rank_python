#!/usr/bin/env python3
# https://www.hackerrank.com/interview/interview-preparation-kit/strings/challenges
import math
import os
import random
import re
import sys

# Complete the isValid function below.
def isValid(s):
    d=dict()
    for i in s:
        if i in d:
            d[i]+=1
        else:
            d[i]=1
    occurs=list(d.values())
    m=max(occurs)
    n=min(occurs)
    if m-n > 1:
        return "NO"
    elif occurs.count(m) > 1:
        return "NO"
    else:
        return "YES"

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    result = isValid(s)

    fptr.write(result + '\n')

    fptr.close()
