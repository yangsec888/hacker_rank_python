#!/usr/bin/env python3
# https://www.hackerrank.com/challenges/ctci-merge-sort/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=sorting
import math
import os
import random
import re
import sys

def file_2_list(file):
    my_list=[]
    with open(file,'r') as f:
        line=f.readline()
        while line:
            my_list.append(line.rstrip())
            line=f.readline()
    f.close()
    return my_list
    
def findMax(arr):
    max=(0,arr[0])
    for i,v in enumerate(arr):
        if v> max[1]:
            max=(i,v)
    return max

# Complete the countInversions function below.
def countInversions(arr):
    cnt=0
    while len(arr)>1:
        arr.reverse()
        (idx,cur_max) = findMax(arr)
        cnt += idx
        del arr[idx]
        arr.reverse()
    return cnt

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')
    input_arr = file_2_list(input())
    t = int(input_arr[0])

    i = 0
    for t_itr in range(t):
        n = int(input_arr[i+1])

        arr = list(map(int, input_arr[i+2].rstrip().split()))

        result = countInversions(arr)

        fptr.write(str(result) + '\n')
        i+=2

    fptr.close()
