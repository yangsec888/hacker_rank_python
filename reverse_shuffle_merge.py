#!/bin/python3
# https://www.hackerrank.com/challenges/reverse-shuffle-merge/forum/comments/498596
# https://www.hackerrank.com/challenges/reverse-shuffle-merge/forum/comments/755493

import math
import os
import random
import re
import sys
import string

# Complete the reverseShuffleMerge function below.
def reverseShuffleMerge(s):
    result=list()
    a_dict = {x:0 for x in string.ascii_lowercase}
    for i in s:
        a_dict[i] = a_dict.get(i,0)+1
    need = {k:v//2 for k,v in a_dict.items()}
    shuffle = {k:v//2 for k,v in a_dict.items()}
    # parse the reverse 's' one at a time to search for 'minimal' lexical sub string
    for char in reversed(s):
        if need[char] >0:
            while result and result[-1] > char and shuffle[result[-1]] >0:
                removed=result.pop()
                need[removed] += 1
                shuffle[removed] -= 1
            result.append(char)
            need[char] -= 1
        else:
            shuffle[char] -= 1
    return ''.join(result)

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    result = reverseShuffleMerge(s)
    print(result)
    fptr.write(result + '\n')

    fptr.close()
