#!/usr/bin/env python3
# https://www.hackerrank.com/challenges/ctci-making-anagrams/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=strings

import math
import os
import random
import re
import sys
import string

# Complete the makeAnagram function below.
def makeAnagram(a, b):
    cnt = 0
    dict_a = dict()
    my_string = string.ascii_lowercase
    for i in list(my_string):
        dict_a.update({i:0})
    dict_b = dict_a.copy()
    for j in list(a):
        dict_a[j] += 1
    for k in list(b):
        dict_b[k] += 1
    for l in my_string:
        cnt += abs(dict_a[l] - dict_b[l])
    return cnt
    
if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    a = input()

    b = input()

    res = makeAnagram(a, b)

    fptr.write(str(res) + '\n')

    fptr.close()
