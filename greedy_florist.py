#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the getMinimumCost function below.
def getMinimumCost(k, c):
    score=0
    c.sort()
    c.reverse()
    ts = n//k
    r = n%k
    #print("ts:",ts, "r:",r)
    for i in range(ts):             # number of round
        for j in range(k):          # number of friend
            idx = i*k + j
            score+= c[idx]*(i+1)
            #print("Processing:",c[idx], "Score:",score)
    for l in c[n-r:]:
        score+=(ts+1)*l
    return score


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    nk = input().split()

    n = int(nk[0])

    k = int(nk[1])

    c = list(map(int, input().rstrip().split()))

    minimumCost = getMinimumCost(k, c)
    print(minimumCost)
    fptr.write(str(minimumCost) + '\n')

    fptr.close()
