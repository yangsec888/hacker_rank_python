# https://www.hackerrank.com/interview/interview-preparation-kit/warmup/challenges

#Sample Input

# 8
# UDDDUDUU

# Complete the counting function
def count(n,s):
    valey_cnt=0
    valey_start=False
    valey_end=False
    cur_level=pre_level=0
    steps = list(s)
    for x in range(n):
        cur_step = steps[x]
        #print("Step:",x, "Curent Step: ", cur_step)
        pre_level = cur_level
        if cur_step=="U":
            cur_level= cur_level+1
        else:
            cur_level= cur_level-1
        if pre_level == 0 and cur_level < pre_level:
            valey_start=True
            valey_end = False
        if cur_level == 0 and cur_level > pre_level:
            valey_end = True
            valey_start = False
            valey_cnt+=1
        #print("Current sea level: ", cur_level)
    return valey_cnt

if __name__ == '__main__':

    n = int(input())
    s = str(input())
    result = count (n, s)
    print(result)
