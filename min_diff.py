#!/bin/python3
#  https://www.hackerrank.com/challenges/minimum-absolute-difference-in-an-array/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=greedy-algorithms

import math
import os
import random
import re
import sys

# Complete the minimumAbsoluteDifference function below.
def minimumAbsoluteDifference(arr):
    diff_arr= list()
    arr.sort()
    pre=arr[0]
    for i in range(1,len(arr)):
        diff_arr.append(arr[i]-pre)
        pre=arr[i]
    return min(diff_arr)


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    arr = list(map(int, input().rstrip().split()))

    result = minimumAbsoluteDifference(arr)
    print(result)
    fptr.write(str(result) + '\n')

    fptr.close()
