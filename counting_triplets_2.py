def countTriplets(arr, r):
    # initialize the dictionaries
    r2 = {}
    r3 = {}
    count = 0

    # loop throgh the arr itens
    for elm in arr:
        # if elm in r3 indicates the triplet already completed,
        # the count need be incremented
        if elm in r3:
            count += r3[elm]

        # if elm in r2, it is the secound number of the triplet,
        # your successor (third element elm*r) need be added or incremented in the r3 dict
        # because is a potencial triplet
        if elm in r2:
            if elm*r in r3:
                r3[elm*r] += r2[elm]
            else:
                r3[elm*r] = r2[elm]

        # else, elm is the first element of the triplet, so,
        # your seccessor (secound element elm*r) need be added or incremented in the r2 dict
        # because is a potencial triplet
        if elm*r in r2:
            r2[elm*r] += 1
        else:
            r2[elm*r] = 1
        #print("elm:", elm, "r2: ",r2, "r3:", r3, "count:", count)
    return count

if __name__ == '__main__':
    inputs = input().strip().split()
    n = int(inputs[0])
    r = int(inputs[1])
    arr = list(map(int, input().rstrip().split()))
    print(countTriplets(arr,r))
