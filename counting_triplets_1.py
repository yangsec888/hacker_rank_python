import os
import sys

# Complete the countTriplets function below.
from collections import Counter

def countTriplets(arr, r):
    r2 = Counter()
    r3 = Counter()
    count = 0
    
    for v in arr:
        if v in r3:
            count += r3[v]

        if v in r2:
            r3[v*r] += r2[v]

        r2[v*r] += 1

    return count

if __name__ == '__main__':
    inputs = input().strip().split()
    n = int(inputs[0])
    r = int(inputs[1])
    arr = list(map(int, input().rstrip().split()))
    print(countTriplets(arr,r))
