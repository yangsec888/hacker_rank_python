#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the whatFlavors function below.
def whatFlavors(cost, money):
    result = list()
    price = cost.copy()
    price.sort()
    bk_idx=0
    for idx in range(1,len(price)):
        if price[idx-1]>=money and price[idx]<=money:
            bk_idx=idx
            break
    print("price:",price)
    print("money:",money)
    print("bk_idx:",bk_idx)
    for i in range(bk_idx,len(price)-1):
        for j in range(i,len(price)):
            if price[i]+price[j] == money:
                result.append(cost.index(price[i])+1)
                if price[i]==price[j]:
                    result.append(cost.index(price[i])+2)
                else:
                    result.append(cost.index(price[j])+1)
                result.sort()
                res=" ".join(map(str,result))
                print(res)
                return 0



if __name__ == '__main__':
    t = int(input())

    for t_itr in range(t):
        money = int(input())

        n = int(input())

        cost = list(map(int, input().rstrip().split()))

        whatFlavors(cost, money)
