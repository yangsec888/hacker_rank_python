#!/bin/python3
# https://www.hackerrank.com/interview/interview-preparation-kit/sorting/challenges

import math
import os
import random
import re
import sys

# conveert file to a list
def file_2_list(file):
    my_list=[]
    with open(file,'r') as f:
        line=f.readline()
        while line:
            my_list.append(line.rstrip())
            line=f.readline()
    f.close()
    return my_list

# custom binary sort algo
def binary_sort(window):
    global z
    b = z.copy()
    for j in window:
        b[j] = b[j] + 1
    c = []
    for idx,val in enumerate(b):
        if val > 0:
            for _ in range(val):
                c.append(idx)
    return c

# Complete the fraud_notice function below.
def fraud_notice(a):
    global n,d
    cnt=0
    for idx in range(d,n):
        exp = a[idx]
        exps = a[idx-d:idx]
        exps = binary_sort(exps)
        if d%2 == 1:
            med=exps[d//2]
        else:
            med=(exps[d//2-1]+exps[d//2])/2
        if exp >= 2*med:
            cnt+=1
    return cnt


if __name__ == '__main__':
    z = [ 0 for i in range(201) ]
    file=str(input())
    input_list= file_2_list(file)
    [n,d] = list(map(int, input_list[0].split(' ')))
    a = list(map(int, input_list[1].rstrip().split(' ')))
    print(fraud_notice(a))
