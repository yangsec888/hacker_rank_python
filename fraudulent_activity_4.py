#!/bin/python3
# https://www.hackerrank.com/interview/interview-preparation-kit/sorting/challenges
# https://www.geeksforgeeks.org/bisect-algorithm-functions-in-python/

import math
import os
import random
import re
import sys
from bisect import bisect_left, insort_left

# conveert file to a list
def file_2_list(file):
    my_list=[]
    with open(file,'r') as f:
        line=f.readline()
        while line:
            my_list.append(line.rstrip())
            line=f.readline()
    f.close()
    return my_list

# Complete the fraud_notice function below.
def fraud_notice(a):
    global n,d
    cnt=0
    for idx in range(d,n):
        exp = a[idx]
        if idx==d:
            exps = a[idx-d:idx]
            exps.sort()
        else:
            exp0 = a[idx-d-1]
            exp1 = a[idx-1]
            del exps[bisect_left(exps,exp0)]
            insort_left(exps,exp1)
        if d%2 == 1:
            med = exps[d//2]
        else:
            med = (exps[d//2-1]+exps[d//2]) / 2
        if exp >= 2*med:
            cnt+=1
    return cnt


if __name__ == '__main__':
    file=str(input())
    input_list= file_2_list(file)
    #print("Input_list",input_list)
    [n,d] = list(map(int, input_list[0].split(' ')))
    a = list(map(int, input_list[1].rstrip().split(' ')))
    print(fraud_notice(a))
