#!/bin/python3
# https://www.hackerrank.com/challenges/special-palindrome-again/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=strings

import math
import os
import random
import re
import sys

# solution explanation: https://medium.com/@carlosbf/special-palindrome-again-solution-80a31ef3c26c
# Complete the substrCount function below.
def substrCount(n, s):
    total=0
    l1 = list(s)
    l2 = list()
    pre = l1[0]
    cnt1 = 1
    for i in range(1,n):
        if l1[i] == pre:
            cnt1+=1
        else:
            l2.append({pre:cnt1})
            pre = l1[i]
            cnt1 = 1
    l2.append({pre:cnt1})
    # case #1
    for item in l2:
        for k,v in item.items():
            total+=v*(v+1)//2
    # case #2
    l_k=""
    l_v=0
    m_k=""
    m_v=0
    for item in l2:
        for k,v in item.items():
            if m_v==1:
                if l_k == k:
                    total+=min(l_v,v)
            l_k=m_k
            l_v=m_v
            m_k=k
            m_v=v
    return total

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    s = input()

    result = substrCount(n, s)
    print(result)

    fptr.write(str(result) + '\n')

    fptr.close()
