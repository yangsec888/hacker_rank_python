#https://www.hackerrank.com/challenges/diagonal-difference/problem?h_r=next-challenge&h_v=zen&h_r=next-challenge&h_v=zen

import math
import os
import random
import re
import sys

def diagonalDifference(arr):
    # Write your code here
    a=b=0
    l=len(arr)
    [x1,y1]=[0,0]
    [x2,y2]=[l-1,0]
    for i in range(l):
        a+=arr[x1][y1]
        b+=arr[x2][y2]
        x1+=1
        y1+=1
        x2-=1
        y2+=1
    return abs(a-b)


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')
    n = int(input().strip())
    arr = []
    for _ in range(n):
        arr.append(list(map(int, input().rstrip().split())))

    result = diagonalDifference(arr)
    fptr.write(str(result) + '\n')
    fptr.close()
