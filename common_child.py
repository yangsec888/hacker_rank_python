#!/bin/python3
# https://www.hackerrank.com/interview/interview-preparation-kit/strings/challenges
# https://www.geeksforgeeks.org/longest-common-subsequence-dp-4/

import math
import os
import random
import re
import sys
sys.setrecursionlimit(100000)
import functools

# Decorator
def memoize(func):
    cache = func.cache = {}
    @functools.wraps(func)
    def memoized_func(*args, **kwargs):
        key = str(args) + str(kwargs)
        if key not in cache:
            cache[key] = func(*args, **kwargs)
        return cache[key]
    return memoized_func

# Complete the commonChild function below.
@memoize
def commonChild(s1, s2, m, n ):
    if m==0 or n==0:
        return 0
    elif s1[m-1] == s2[n-1]:
        return 1 + commonChild(s1, s2, m-1, n-1)
    else:
        return max(commonChild(s1,s2,m,n-1),commonChild(s1,s2,m-1,n))


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s1 = input()

    s2 = input()

    result = commonChild(s1, s2, len(s1), len(s2))
    print(result)
    fptr.write(str(result) + '\n')

    fptr.close()
