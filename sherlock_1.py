#!/usr/bin/env python3
# https://www.hackerrank.com/interview/interview-preparation-kit/strings/challenges
import math
import os
import random
import re
import sys

# Complete the isValid function below.
def isValid(s):
    d,c=dict(),dict()
    for i in s:
        if i in d:
            d[i]+=1
        else:
            d[i]=1
    #print("d:",d)
    for j in list(d.values()):
        c[j] = c.get(j,0)+1
    #print("c:",c)
    n = len(c)
    if n == 1:
        return "YES"
    elif n==2:
        pre_k=0
        for k,v in c.items():
            if k==1 and v==1:
                return "YES"
            elif v==1:
                if k-pre_k==1:
                    return "YES"
            else:
                pre_k=k
    return "NO"

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    result = isValid(s)
    print(result)
    fptr.write(result + '\n')

    fptr.close()
