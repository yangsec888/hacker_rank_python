#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the freqQuery function below.
def freqQuery(queries):
    result=[]
    stack={}
    freq_cnt={}
    for cmd,val in queries:
        if cmd==1:
            if val in stack:
                freq_cnt[stack[val]] -= 1
            stack[val]=stack.get(val,0)+1
            freq_cnt[stack[val]]=freq_cnt.get(stack[val],0)+1
        elif cmd==2:
            if val in stack and stack[val]>0:
                freq_cnt[stack[val]] -= 1
                stack[val]-=1
                freq_cnt[stack[val]] = freq_cnt.get(stack[val],0)+1
        elif cmd==3:
            if val in freq_cnt and freq_cnt[val]:
                result.append(1)
            else:
                result.append(0)
    return result


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    q = int(input().strip())

    queries = []

    for _ in range(q):
        queries.append(list(map(int, input().rstrip().split())))

    ans = freqQuery(queries)

    fptr.write('\n'.join(map(str, ans)))
    fptr.write('\n')

    fptr.close()
