# https://www.hackerrank.com/challenges/sherlock-and-anagrams/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=dictionaries-hashmaps
import os
import sys

# convert file to list
def file_2_list(file):
    my_list=[]
    with open(file,'r') as f:
        line=f.readline()
        while line:
            my_list.append(line.rstrip())
            line=f.readline()
    f.close()
    return my_list

# string to dictionary converstion: 'abba' => {'a':2, 'b':2}
def str_to_dic(s1):
    dict={}
    for x in list(s1):
        if x in dict:
            dict[x]+=1
        else:
            dict[x]=1
    return dict

# anagrams evaluation function
def are_anagrams(s1,s2):
    return True if str_to_dic(s1) == str_to_dic(s2) else False

if __name__ == '__main__':
    file=str(input())
    input_list= file_2_list(file)
    entries = int(input_list[0])
    inputs = []
    for x in range(entries):
        my_string=input_list[x+1]
        #print("Input string:",my_string)
        sub_strs={}
        i=0
        #build full substring in hash map: 'abba' => {1: 'a', 2: 'ab', 3: 'abb', 4: 'abba', 5: 'b', 6: 'bb', 7: 'bba', 8: 'b', 9: 'ba', 10: 'a'}
        for x in range(len(my_string)):
            for y in range(x,len(my_string)):
                sub_strs.update({i:my_string[x:y+1]})
                i+=1
        cnt=0
        #print(sub_strs)
        #iterate through the hash map and count the anagrams once found
        for key1,val1 in sub_strs.items():
            for key2 in range(key1+1,len(sub_strs)):
                if len(val1)==len(sub_strs[key2]):
                    if are_anagrams(val1,sub_strs[key2]):
                        cnt+=1
        print(cnt)
