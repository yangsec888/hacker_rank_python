# https://www.hackerrank.com/challenges/jumping-on-the-clouds/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=warmup

#Sample Input
#6
#0 0 0 0 1 0

#Sample output
# 3

# Complete the counting function
def jump(n,s):
    jump_cnt=0
    idx=0
    steps=[int(x) for x in s.split(' ')]
    #print("steps", steps)
    while idx < n-1:
        #print(idx)
        jump_cnt+=1
        if idx+2==n-1 or idx+1==n-1:
            break
        if steps[idx+2] != 1:
            idx+=2
        elif steps[idx+1] != 1:
            idx+=1
        else:
            print("Error: invalid input - Can't jump over in step ",idx)
            break
    return jump_cnt

if __name__ == '__main__':
    n = int(input())
    s = str(input())
    print(jump(n, s))
