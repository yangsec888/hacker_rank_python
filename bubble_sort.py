#!/bin/python3
# https://www.hackerrank.com/challenges/ctci-bubble-sort/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=sorting

import math
import os
import random
import re
import sys

# Complete the countSwaps function below.
def countSwaps(a):
    cnt=0
    for idx in range(len(a)):
        for idy in range(len(a)-1):
            if a[idy]> a[idy+1]:
                a[idy],a[idy+1]=a[idy+1],a[idy]
                cnt+=1
    print("Array is sorted in",cnt,"swaps.")
    print("First Element:",a[0])
    print("Last Element:",a[-1])
    return a
    
if __name__ == '__main__':
    n = int(input())

    a = list(map(int, input().rstrip().split()))

    countSwaps(a)
