#!/usr/bin/env python3
# https://www.hackerrank.com/challenges/new-year-chaos/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=arrays

import math
import os
import random
import re


# Complete the rotRight function below.
def chaos_sort(stage):
    global bribe_cnt
    global chaotic
    for pos,x in enumerate(stage):
        if (x - pos -1) > 2:
            chaotic=True
            return
        for j in range(max(0,x-2),pos+1):
            if stage[j] > x:
                bribe_cnt+=1

if __name__ == '__main__':
    test_cases=int(input())
    stages=[]
    for _ in range(test_cases):
        size_queue = int(input())
        stages.append(list(map(int, input().rstrip().split()))[:size_queue])
    for stage in stages:
        bribe_cnt=0
        chaotic=False
        chaos_sort(stage)
        if chaotic:
            print("Too chaotic")
        else:
            print(bribe_cnt)
