#!/bin/python3
# https://www.hackerrank.com/challenges/special-palindrome-again/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=strings

import math
import os
import random
import re
import sys

# Function to determine if it's special string
def is_special(sub):
    c=dict()
    for k in sub:
        c[k]=c.get(k,0)+1
    if len(c)==1:
        return True
    elif len(c)>2:
        return False
    elif 1 not in list(c.values()):
        return False
    elif len(sub)%2==0:
        return False
    else:
        for k,v in c.items():
            if v==1:
                if k==list(sub)[len(sub)//2]:
                    return True
                else:
                    return False

# Complete the substrCount function below.
def substrCount(n, s):
    cnt=0
    for i in range(n+1):
        for j in range(i+1,n+1):
            sub=s[i:j]
            #print("sub:",sub)
            if is_special(sub):
                cnt+=1
    return cnt

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    s = input()

    result = substrCount(n, s)
    print(result)

    fptr.write(str(result) + '\n')

    fptr.close()
