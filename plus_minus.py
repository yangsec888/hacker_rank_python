import os
import sys

#
# Complete the plusMinus function below.
#
def plusMinus(arr):
    #
    p=n=z=0
    for x in arr:
        if x > 0:
            p+=1
        elif x==0:
            z+=1
        else:
            n+=1
    print(round(p/len(arr),6))
    print(round(n/len(arr),6))
    print(round(z/len(arr),6))


if __name__ == '__main__':
    n = int(input())

    arr = list(map(int, input().rstrip().split()))

    plusMinus(arr)
